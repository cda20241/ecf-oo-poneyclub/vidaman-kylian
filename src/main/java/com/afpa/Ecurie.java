package com.afpa;

import java.util.ArrayList;
import java.util.List;

import org.javatuples.Pair;

public class Ecurie {

    // __Attribut__//

    private String nom;
    private List<Box> boxes;
    private List<Poney> poneys;
    private List<Salarie> salaries;

    // __Constructeur__//

    public Ecurie() {
        this.nom = null;
        this.boxes = new ArrayList<>();
        this.poneys = new ArrayList<>();
        this.salaries = new ArrayList<>();
    }

    public Ecurie(String nom) {
        this.nom = nom;
        this.boxes = new ArrayList<>();
        this.poneys = new ArrayList<>();
        this.salaries = new ArrayList<>();
    }

    // __Getteur/Setteur__//

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public List<Box> getBoxes() {
        return this.boxes;
    }

    public void setBoxes(List<Box> boxes) {
        this.boxes = boxes;
    }

    public List<Poney> getPoneys() {
        return this.poneys;
    }

    public void setPoneys(List<Poney> poneys) {
        this.poneys = poneys;
    }

    public List<Salarie> getSalaries() {
        return this.salaries;
    }

    public void setSalaries(List<Salarie> salaries) {
        this.salaries = salaries;
    }

    // __Methode__//

    /**
     * Créer un box de taille taille, un capacitéMaxFoin de valeur égale à
     * capacité, et quantitéFoin = valeurCourante puis ajoute a box à la liste de
     * Box de son écurie.
     * 
     * @param taille
     * @param capaciteMax
     * @param valeurCourante
     */
    public void initBox(int taille, int capaciteMax, int valeurCourante) {
        Box box = new Box(taille, capaciteMax, valeurCourante);
        if (!this.getBoxes().isEmpty()) {
            List<Box> subList = this.getBoxes();
            subList.add(box);
            this.setBoxes(subList);
        } else {
            this.getBoxes().add(box);
        }

    }

    /**
     * Créer un box de taille taille, et avec un capacitéMaxFoin de valeur égale à
     * capacité, puis ajoute a box à la liste de Box de son écurie.
     * 
     * @param taille
     * @param capacite
     */
    public void creerBox(int taille, int capacite) {
        Box box = new Box(taille, capacite);
        if (!this.getBoxes().isEmpty()) {
            List<Box> subList = this.getBoxes();
            subList.add(box);
            this.setBoxes(subList);
        } else {
            this.getBoxes().add(box);
        }
    }

    /**
     * Prend en paramètre une instance de Box, cherche dans la liste de Box si elle
     * existe,
     * si oui la supprime de la liste.
     * 
     * @param box
     */
    public void supprimerBox(Box box) {
        List<Box> subList = this.getBoxes();
        if (subList.remove(box)) {
            this.setBoxes(subList);
        }
    }

    /**
     * Prend en paramètre une Personne/Salarie, vérifie si elle ne possède déjà pas
     * une écurie, si non est ajouter à la list de Salarie de l'ecurie, si oui
     * retourne un erreur
     * 
     * @param salarie
     */
    public void recruterEmploye(Salarie salarie) {
        if (salarie.getEcurie() == null) {
            salarie.setEcurie(this);
            List<Salarie> subList = this.getSalaries();
            subList.add(salarie);
            this.setSalaries(subList);
        } else {
            System.err.println("Cette personne est déjà employé par une entreprise.");
        }
    }

    /**
     * Prend en paramètre une instance de Salarie, vérifie si son écurie correspond
     * à notre objet, si oui l'enlève de notre liste de Salarie.
     * 
     * @param salarie
     */
    public void virerEmploye(Salarie salarie) {
        if (salarie != null) {
            if (salarie.getEcurie() == this) {
                salarie.setEcurie(null);
                List<Salarie> subList = this.getSalaries();
                subList.remove(salarie);
                this.setSalaries(subList);
            } else {
                System.err.println("Cette personne ne peut pas être virée si elle ne travaille pas pour notre écurie.");
            }
        } else {
            System.out.println("L'instance d'objet est null");
        }
    }

    /**
     * Prend une instance de Poney en paramètre, si non null, l'ajoute à la liste de
     * Poney de notre écurie
     * 
     * @param poney
     */
    public void acheterPoney(Poney poney) {
        if (poney != null) {
            if (poney.getEcurie() == null) {
                List<Poney> subList = this.getPoneys();
                subList.add(poney);
                this.setPoneys(subList);
                poney.setEcurie(this);
            }
        } else {
            System.err.println("L'instance d'objet est null");
        }
    }

    /**
     * Prend une instance de Poney en paramètre, vérifie si elle est différente de
     * null, puis si elle existe dans la liste de Poney de notre écurie. Si oui
     * l'enlève de la liste
     * 
     * @param poney
     */
    public void vendrePoney(Poney poney) {
        if (poney != null) {
            if (this.getPoneys().contains(poney)) {
                List<Poney> subList = this.getPoneys();
                subList.remove(poney);
                this.setPoneys(subList);
                poney.setEcurie(null);
            }
        } else {
            System.err.println("L'instance d'objet est null");
        }
    }

    /**
     * Prend une instance de Poney en paramètre, vérifie si elle est non null et que
     * son box appartient à notre écurie. Si oui on met la box de l'instance égale à
     * null.
     * 
     * @param poney
     */
    public void envoyerPaitre(Poney poney) {
        if (poney != null) {
            if (this.getBoxes().contains(poney.getBox())) {
                poney.setBox(null);
            }
        } else {
            System.err.println("L'instance d'objet est null.");
        }
    }

    /**
     * Prend une instance de Poney et Box en paramètre, verifie si elles ne sont pas
     * null,
     * puis vérifie certaines conditions qui si remplie, ajoute le poney au box et
     * inversement
     * 
     * @param poney
     * @param box
     */
    public void rentrerPoney(Poney poney, Box box) {
        if (poney != null && box != null) {
            // verifie si la box et le poney appartiènent bien à l'écurie, et si il reste de
            // la place dans le box pour le poney.
            if (this.getBoxes().contains(box) && poney.getEcurie() == this
                    && box.getPoneys().size() < box.getCapacitePoney()) {
                poney.setBox(box);
                List<Poney> subList = box.getPoneys();
                subList.add(poney);
                box.setPoneys(subList);
            }
        } else {
            System.err.println("L'instance d'objet est null.");
        }
    }

    /**
     * Récupère toutes les pourcentages de remplissage des boxes de l'ecurie et les
     * affiches.
     * 
     * @return
     */
    public void afficherRemplissageDesBox() {
        int i = 1;
        int capMax;
        int cap;
        int pourcentage;
        System.out.println("Taux de remplissage des boxes:");
        for (Box box : this.getBoxes()) {
            capMax = box.getCapaciteFoin();
            cap = box.getQuantiteFoin();
            pourcentage = cap * 100 / capMax;
            System.out.println("Box " + i + ": " + pourcentage + "%, soit " + cap + " pour une capacité de " + capMax);
            i++;
        }
    }

    /**
     * Renvoie 2 integer composé de la somme de toute les capacitéMaxFoin de boxes
     * et de la quantiteFoin sous la forme d'un tuple.
     * 
     * @return
     */
    public Pair<Integer, Integer> sommeVal() {
        int somCapMax = 0;
        int somCap = 0;
        for (Box box : this.getBoxes()) {
            somCapMax += box.getCapaciteFoin();
            somCap += box.getQuantiteFoin();
        }
        return new Pair<>(somCapMax, somCap);
    }

    /**
     * Utilise le pourcentage pour egaliser le taux de chaque box, puis sort les
     * valeurs restantes sous int
     * 
     * @param pourcentage
     * @param reste
     * @return
     */
    public int distribEgale(int pourcentage, int reste) {
        int newRest = reste;
        for (Box box : this.getBoxes()) {
            int val = (box.getCapaciteFoin() * pourcentage) / 100;
            newRest -= val;
            box.setQuantiteFoin(val);
        }
        return newRest;
    }

    /**
     * Prend une livraison, verifie si elle à bien été receptionné, si oui,
     * distribue sa quantité de foin en egalisant le taux des réserves
     * 
     * @param livraison
     */
    public void repartirFoin(Livraison livraison) {
        if (livraison.getReceptionniste() != null) {
            int foinLivraison = livraison.getFoin().getQuantite();
            Pair<Integer, Integer> somVal = this.sommeVal();
            int somCapMax = somVal.getValue0();
            int somCap = somVal.getValue1();
            int pourcentage = (somCap + foinLivraison) * 100 / somCapMax;
            int reste = somCap + foinLivraison;
            int val = this.distribEgale(pourcentage, reste) / this.getBoxes().size();
            for (Box box : this.getBoxes()) {
                box.setQuantiteFoin(box.getQuantiteFoin() + val);
            }
        }
    }
}
