package com.afpa;

public class Criniere {

    enum TEXTURE {
        FRISER,
        LISSE
    }

    // __ Attribut__//

    private int longueur;
    private TEXTURE texture;

    // __Constructeur__//

    public Criniere() {
        this.longueur = 0;
        this.texture = null;
    }

    public Criniere(int longueur, TEXTURE texture) {
        this.longueur = longueur;
        this.texture = texture;
    }

    // __Getteur/Setteur__//

    public int getLongueur() {
        return this.longueur;
    }

    public void setLongueur(int longueur) {
        this.longueur = longueur;
    }

    public TEXTURE getTexture() {
        return this.texture;
    }

    public void setTexture(TEXTURE texture) {
        this.texture = texture;
    }

}
