package com.afpa;

public class Foin {

    enum TYPE {
        PRAIRIE,
        GRAMINEES
    }

    // __Attribut__//
    private String numeroLot;
    private Integer quantite;
    private TYPE type;

    // __Constructeur__//

    public Foin() {
        this.numeroLot = null;
        this.quantite = null;
        this.type = null;
    }

    public Foin(String numeroLot, Integer quantite, TYPE type) {
        this.numeroLot = numeroLot;
        this.quantite = quantite;
        this.type = type;
    }

    // __Getteur/Setteur__//

    public String getNumeroLot() {
        return this.numeroLot;
    }

    public void setNumeroLot(String numeroLot) {
        this.numeroLot = numeroLot;
    }

    public Integer getQuantite() {
        return this.quantite;
    }

    public void setQuantite(Integer quantite) {
        this.quantite = quantite;
    }

    public TYPE getType() {
        return this.type;
    }

    public void setType(TYPE type) {
        this.type = type;
    }

}
