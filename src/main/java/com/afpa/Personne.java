package com.afpa;

public class Personne {

    // __Attribut__ //

    private String nom;
    private String prenom;

    //__Constructor__//

    public Personne() {
        this.nom = null;
        this.prenom = null;
    }

    public Personne(String nom, String prenom) {
        this.nom = nom;
        this.prenom = prenom;
    }

    // __getteur/setteur__ //

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return this.prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

}
