package com.afpa;

import java.util.ArrayList;
import java.util.List;

public class Box {

    // __Attribut__//
    private int capacitePoney;
    private List<Poney> poneys;
    private int capaciteFoin;
    private int quantiteFoin;

    // __Constructeur__//

    public Box(int capacitePoney, int capaciteFoin) {
        this.capacitePoney = capacitePoney;
        this.poneys = new ArrayList<>();
        this.capaciteFoin = capaciteFoin;
        this.quantiteFoin = 0;
    }

    public Box(int capacitePoney, int capaciteFoin, int quantiteFoin) {
        this.capacitePoney = capacitePoney;
        this.poneys = new ArrayList<>();
        this.capaciteFoin = capaciteFoin;
        this.quantiteFoin = quantiteFoin;
    }

    // __Getteur/Setteur__//

    public int getCapacitePoney() {
        return this.capacitePoney;
    }

    public void setCapacitePoney(int capacitePoney) {
        this.capacitePoney = capacitePoney;
    }

    public List<Poney> getPoneys() {
        return this.poneys;
    }

    public void setPoneys(List<Poney> poneys) {
        this.poneys = poneys;
    }

    public int getCapaciteFoin() {
        return this.capaciteFoin;
    }

    public void setCapaciteFoin(int capaciteFoin) {
        this.capaciteFoin = capaciteFoin;
    }

    public int getQuantiteFoin() {
        return this.quantiteFoin;
    }

    public void setQuantiteFoin(int quantiteFoin) {
        this.quantiteFoin = quantiteFoin;
    }

}
