package com.afpa;

public class Salarie extends Personne {
    
    enum FUNCTION {
        RECEPTIONNISTE,
        PALEFRENIER
    }

    //__Attribut__//

    private FUNCTION function;
    private Ecurie ecurie;

    //__Constructeur__//

    public Salarie(){
        super();
        this.function = null;
        this.ecurie = null;
    }

    public Salarie(String nom, String prenom, FUNCTION function) {
        super(nom, prenom);
        this.function = function;
        this.ecurie = null;
    }

    public Salarie(String nom, String prenom, FUNCTION function, Ecurie ecurie) {
        super(nom, prenom);
        this.function = function;
        this.ecurie = ecurie;
    }

    //__Getteur/Setteur__//

    public FUNCTION getFunction() {
        return this.function;
    }

    public void setFunction(FUNCTION function) {
        this.function = function;
    }

    public Ecurie getEcurie() {
        return this.ecurie;
    }

    public void setEcurie(Ecurie ecurie) {
        this.ecurie = ecurie;
    }

}
