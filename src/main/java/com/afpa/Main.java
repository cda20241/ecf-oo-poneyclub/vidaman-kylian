package com.afpa;

import java.util.ArrayList;
import java.util.List;

import com.afpa.Criniere.TEXTURE;
import com.afpa.Foin.TYPE;
import com.afpa.Salarie.FUNCTION;

public class Main {

    public static void main(String[] args) {

        List<Personne> allPersonne = new ArrayList<>();
        List<Poney> allPoney = new ArrayList<>();
        List<Livraison> allLivraisons = new ArrayList<>();

        Salarie salarie1 = new Salarie("KENOBI", "Obiwan", FUNCTION.RECEPTIONNISTE);
        Salarie salarie2 = new Salarie("SKYWALKER", "Luc", FUNCTION.PALEFRENIER);

        allPersonne.add(salarie1);
        allPersonne.add(salarie2);

        Ecurie ecurie = new Ecurie("Tatouine Poney Club");

        ecurie.recruterEmploye(salarie1);
        ecurie.recruterEmploye(salarie2);

        ecurie.initBox(3, 600, 200);
        ecurie.initBox(4, 600, 300);
        ecurie.initBox(5, 1000, 900);
        ecurie.initBox(6, 800, 100);

        Poney poney1 = new Poney("Usain Bolt", 250, 25, TEXTURE.LISSE);
        Poney poney2 = new Poney("Petit Tonnerre", 280, 28, TEXTURE.FRISER);

        allPoney.add(poney1);
        allPoney.add(poney2);

        ecurie.acheterPoney(poney1);
        ecurie.acheterPoney(poney2);

        Livraison livraison = new Livraison("F003", 200, TYPE.PRAIRIE);
        allLivraisons.add(livraison);
        livraison.receptionnerFoin(salarie1);

        ecurie.afficherRemplissageDesBox();
        ecurie.repartirFoin(livraison);
        ecurie.afficherRemplissageDesBox();

        System.out.println(Livraison.dateLivraison(allLivraisons, "F003"));
    }
}