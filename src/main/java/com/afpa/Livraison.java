package com.afpa;

import java.util.List;

import com.afpa.Foin.TYPE;

public class Livraison {

    // __Attribut__//

    private String dateLivraison;
    private Foin foin;
    private Salarie receptionniste;

    // __Constructeur__//

    public Livraison() {
        this.dateLivraison = null;
        this.foin = new Foin();
        this.receptionniste = null;
    }

    public Livraison(String numLot, int quantite, TYPE type) {
        this.dateLivraison = null;
        this.foin = new Foin(numLot, quantite, type);
        this.receptionniste = null;
    }

    // __Getteur/Setteur__//

    public String getDateLivraison() {
        return this.dateLivraison;
    }

    public void setDateLivraison(String dateLivraison) {
        this.dateLivraison = dateLivraison;
    }

    public Foin getFoin() {
        return this.foin;
    }

    public void setFoin(Foin foin) {
        this.foin = foin;
    }

    public Salarie getReceptionniste() {
        return this.receptionniste;
    }

    public void setReceptionniste(Salarie receptionniste) {
        this.receptionniste = receptionniste;
    }

    // __Methode__//

    /**
     * Prend une instance de Livraison, vérifie si elle à déjà été receptionner, si
     * non l'attribut receptionniste est égale au paramètre d'entrer receptionniste,
     * la dateLivraison correspond à la date récupérer par l'OS. Puis on ajoute la
     * livraison au registre de l'écurie.
     * 
     * @param recepetionniste
     */
    public void receptionnerFoin(Salarie recepetionniste) {
        if (this.getReceptionniste() == null) {
            this.setReceptionniste(recepetionniste);
            this.setDateLivraison(java.time.LocalDate.now().toString());
        } else {
            System.out.println("Cette livraison à déjà été receptionné.");
        }
    }

    /**
     * Prend en paramètre la liste de toutes les livraisons effectuéent, ainsi
     * qu'une instance de Foin, vérifié sa présence dans la liste grâce à son numero
     * de lot, return sa date de livraison si oui, si non return null
     * 
     * @param listeLivraisons
     * @param foin
     * @return String or null
     */
    public static String dateLivraison(List<Livraison> listeLivraisons, String numLot) {
        String numeroLot = numLot;
        for (Livraison livraison : listeLivraisons) {
            if (livraison.getFoin().getNumeroLot().equals(numeroLot)) {
                return livraison.dateLivraison;
            }
        }
        return null;
    }

}
