package com.afpa;

import com.afpa.Criniere.TEXTURE;

public class Poney {

    // __Attribut__//

    private String nom;
    private int poids;
    private Criniere criniere;
    private Ecurie ecurie;
    private Box box;
    // __Construceur__//

    public Poney() {
        this.criniere = new Criniere();
    }

    public Poney(String nom, int poids) {
        this.nom = nom;
        this.poids = poids;
        this.criniere = new Criniere();
    }

    public Poney(String nom, int poids, int longueur, TEXTURE texture) {
        this.nom = nom;
        this.poids = poids;
        this.criniere = new Criniere(longueur, texture);
    }

    // __Getteur/Setteur__//

    public String getNom() {
        return this.nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getPoids() {
        return this.poids;
    }

    public void setPoids(int poids) {
        this.poids = poids;
    }

    public Criniere getCriniere() {
        return this.criniere;
    }

    public void setCriniere(Criniere criniere) {
        this.criniere = criniere;
    }

    public Ecurie getEcurie() {
        return this.ecurie;
    }

    public void setEcurie(Ecurie ecurie) {
        this.ecurie = ecurie;
    }

    public Box getBox() {
        return this.box;
    }

    public void setBox(Box box) {
        this.box = box;
    }

}
